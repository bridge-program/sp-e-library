package library.controller;

import library.model.Book;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class BookController {

    @GetMapping("/books")
    public List<Book> allBooks() {
        return Arrays.asList(new Book("Test-Driven Development by Example", "Kent Beck"),
                new Book("Refactoring", "Martin Fowler"));
    }

    @GetMapping("/")
    public String healthcheck() {
        return System.getenv("HOST");
    }
}
