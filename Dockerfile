FROM openjdk:8-alpine
RUN adduser -D myuser
USER myuser
ADD build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar", "app.jar"]
